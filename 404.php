
<!DOCTYPE html>
<html lang="pt-br">
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="author" content="Rodrigo Debossans">
		<meta http-equiv="cache-control" content="no-cache">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" href="template/src/css/main.css">	
		<title>Projeto Celeritech</title>


	</head>

	<body>
		<div class="container-fluid h-100">
			<div class="row flex-xl-nowrap">
				<aside id="sidebar" class="col-12 col-sm-12 col-md-2 col-xl-2 p-0 bg-secondary">
					<!-- MAIN MENU -->
					<?php include(dirname(__FILE__).'/template/modules/menu.php'); ?>
					<!-- END MAIN MENU -->
				</aside>

				<section class="col-12 col-sm-12 col-md-10 col-xl-10">

					<!-- MAIN CONTENT -->
					<main class="p-0" role="main">

						<!-- PAGE IDENTIFY -->
						<div class="row bg-secondary p-0 p-sm-4 p-md-4 p-lg-4 p-xl-4 min-h-top justify-content-between">
							<div class="col-12 col-sm-8 col-md-8 col-xl-8 pt-3 mb-2 mb-sm-0 mb-md-0 mb-lg-0 mb-xl-0">
								<h4 class="text-white font-weight-bold ">Error 404</h4>
							</div>
							<div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 pt-2 d-none d-sm-none d-md-block d-lg-block d-xl-block">
								<form action="#" class="form-inline">
									<div class="form-group has-search w-100">
										<input id="pokeSearch" name="pokeSearch" type="search" class="form-control w-100" placeholder="Seach pokémon" required autocomplete="off">
									</div>							
								</form>
							</div>
						</div>
						<!-- END PAGE IDENTIFY -->

						<hr class="ml-0 mr-0 mt-4 m-sm-4 m-md-4 m-lg-4 m-xl-4">

						<!-- POKEMONS -->
						<div id="loadListPoke">
							<p class="text-center">Page not found</p>
						</div>
						<!-- END POKEMONS -->
	
						<hr class="ml-0 mr-0 mt-3 m-sm-4 m-md-4 m-lg-4 m-xl-4">
			
					</main>
					<!-- END MAIN CONTENT -->

				</section>

			</div>
		</div>

		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	</body>
</html>