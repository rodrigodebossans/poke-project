<?php 
	$url = ( isset($_GET['url']) ) ? $_GET['url']: 'home';
	$url = array_filter(explode('/',$url));

	$file = dirname(__FILE__).'/'.$url[0].'.php';
	$notFound = dirname(__FILE__).'/404.php';

	if (is_file($file)) {
		include $file;
	} else {
		include $notFound;
	}

?>


