
		<nav class="nav navbar navbar-expand-lg navbar-dark flex-column p-0">
			<li class="nav-item bg-primary p-4 min-h-top w-100">
				<a href="#">
					<img id="logo" class="img-fluid mx-auto float-left float-sm-left float-md-none float-lg-none float-xl-none d-block" src="template/img/logo.svg" alt="logo project" title="Home">
				</a>
				<button class="navbar-toggler border-white float-right float-sm-right float-md-none d-lg-none d-xl-none mx-auto d-block mt-2" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Alterna navegação">
					<span class="navbar-toggler-icon text-white"></span>
				</button>
			</li>
	
			<div class="collapse navbar-collapse w-100" id="menu">
				<li class="nav-item mt-4 bg-primary">
					<a href="#" class="nav-link text-light p-3">Pokémon list</a>
				</li>
				<li class="nav-item mb-4 mb-sm-0 mb-md-0 md-lg-0 mb-xl-0">
					<a href="#" class="nav-link text-light p-3">About me</a>
				</li>
			</div>
		</nav>

	