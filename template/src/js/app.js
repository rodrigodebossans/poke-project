
/******************************
    Author: Rodrigo Debossans                  
    Date: 07/2019                             
****************/

$(document).ready(function(){

    let core = {
        getAjax: function(coreGetUrl, coreGetMethod, coreGetDataType, preloader) {
            resp = {};
            $.ajax({
                url: coreGetUrl,
                async: false, // :(
                cache: true,
                type: coreGetMethod,
                dataType: coreGetDataType,
                beforeSend: function() {        
                    if (preloader === true) {
                        console.log('loading...');
                        //$('#loadListPoke').append('<img id="loader" src="template/img/loader.svg" alt="loading..." class="d-block mx-auto">');
                    }
                },
                success: function(resultText) {
                    resp = resultText;
                },
                error: function(e){
                    resp = e;
                }
            });
            return resp;
        },
        getAjaxAsync: function(coreGetUrl, coreGetMethod, coreGetDataType, callback, preloader) {
            $.ajax({
                url: coreGetUrl,
                async: true, // :)
                cache: false,
                type: coreGetMethod,
                dataType: coreGetDataType,
                beforeSend: function() {        
                   if (preloader === true) {
                        console.log('loading...');
                    }
                },
                success: function(e){
                    callback(e);
                },
                error: function(){
                    console.log('error');
                }
            });
        },
        getPokemonTypes: function(type) {
            if ((type != 'undefined') && (type != null)) {
                tmpType = type;
            } else {
                tmpType = '';
            }
            let getPokeTypes = this.getAjax('https://pokeapi.co/api/v2/type/'+tmpType, 'get', 'json');
            return getPokeTypes;
        },
        getPokemonTypesByGeration: function(geration) {
            let getPokeTypesByGeration = this.getAjax('https://pokeapi.co/api/v2/generation/'+geration,'get','json');
            return getPokeTypesByGeration.types;
        },
        getPokemonByGeration: function(geration) {
            let getPokeTypesByGeration = this.getAjax('https://pokeapi.co/api/v2/generation/'+geration,'get','json');
            return getPokeTypesByGeration.pokemon_species;
        },
        getPokemonList: function(geration, offset, limit, type, pagination) {

            let tmpPokeObj = {};
            let tmpPokeObjList = [];
            let tmpListPokemonsByGeration = [];

            // default value geration
            if ((geration != 'undefined') && (geration != null)) {
                tmpGeration = geration;
            } else {
                tmpGeration = 1;
            }
            // default value offset
            if ((offset != 'undefined') && (offset != null)) {
                tmpOffset = offset;
            } else {
                tmpOffset = 0;
            }
            // default value limit
            if ((limit != 'undefined') && (limit != null)) {
                tmpLimit = limit;
            } else {
                tmpLimit = 20;
            }

            localStorage.setItem('offset', tmpOffset);
            localStorage.setItem('limit', tmpLimit);

            // default value type
            if ((type != 'undefined') && (type != null)) {
                tmpType = type;
                tmpListFilterPokemonsByType = [];
                let getPokesTypesInfo = this.getPokemonTypes(tmpType);

                $(getPokesTypesInfo.pokemon).each(function(i, value) {
                    tmpListFilterPokemonsByType.push(value.pokemon.name);
                });
                let valueItemsPagination = Math.ceil(tmpListFilterPokemonsByType.length/tmpLimit);
                if (pagination === true) {
                    this.setPagination(valueItemsPagination);
                }
                for (i=tmpOffset; i<tmpOffset+tmpLimit; i++) {
                    tmpPokeObj.url = 'https://pokeapi.co/api/v2/pokemon-species/'+tmpListFilterPokemonsByType[i];

                    let getPokesGerationInfo = this.getAjax(tmpPokeObj.url, 'get', 'json', true);

                    tmpListPokemonsByGeration.push(getPokesGerationInfo.name);

                    tmpPokeObj.id = getPokesGerationInfo.id;
                    tmpPokeObj.sprite = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'+tmpPokeObj.id+'.png';
                    tmpPokeObj.name = getPokesGerationInfo.name;
                    $(getPokesGerationInfo.flavor_text_entries).each(function(i, value) {
                        if (value.version.name == 'red') {
                            tmpPokeObj.flavor_text_entries = value.flavor_text;
                        }
                    });
                    tmpPokeObjList.push(JSON.stringify(tmpPokeObj)); 
                }
            } else {
                tmpType = '';

                let getPokesGeration = this.getAjax('https://pokeapi.co/api/v2/generation/'+tmpGeration,'get','json');
                let valueItemsPagination = Math.ceil(getPokesGeration.pokemon_species.length/tmpLimit);
                if (pagination === true) {
                    this.setPagination(valueItemsPagination);
                }
                for (i=tmpOffset; i<tmpOffset+tmpLimit; i++) {
                    tmpPokeObj.url = getPokesGeration.pokemon_species[i].url;

                    let getPokesGerationInfo = this.getAjax(tmpPokeObj.url, 'get', 'json', true);

                    tmpListPokemonsByGeration.push(getPokesGerationInfo.name);

                    tmpPokeObj.id = getPokesGerationInfo.id;
                    tmpPokeObj.sprite = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'+tmpPokeObj.id+'.png';
                    tmpPokeObj.name = getPokesGerationInfo.name;
                    $(getPokesGerationInfo.flavor_text_entries).each(function(i, value) {
                        if (value.version.name == 'red') {
                            tmpPokeObj.flavor_text_entries = value.flavor_text;
                        }
                    });

                    tmpPokeObjList.push(JSON.stringify(tmpPokeObj)); 
                }
            }
            return tmpPokeObjList;
        },
        setPagination: function(dataNumber) {
            $('#pagination').html('');
            if ( (dataNumber != 'undefined') && (dataNumber != null) ) {
                let elements = '';
                if (localStorage.getItem('offset') > 0) {
                    elements = '<li id="preview" class="page-item">';
                    elements += '<a class="page-link text-dark preview" href="#" aria-label="Preview">';
                    elements += '<span aria-hidden="true">&laquo;</span>';
                    elements += '<span class="sr-only">Anterior</span>';
                    elements += '</a>';
                    elements += '</li>';
                } else {
                    elements = '<li id="preview" class="page-item d-none">';
                    elements += '<a class="page-link text-dark preview" href="#" aria-label="Preview">';
                    elements += '<span aria-hidden="true">&laquo;</span>';
                    elements += '<span class="sr-only">Anterior</span>';
                    elements += '</a>';
                    elements += '</li>';
                }
                for (i=1; i<=dataNumber; i++) {
                    elements += '<li class="page-item"><button class="page-link text-dark itemPagination" value="'+i+'">'+i+'</button></li>';
                }
                elements += '<li id="next" class="page-item">';
                elements += '<a class="page-link text-dark next" href="#" aria-label="Próximo">';
                elements += '<span aria-hidden="true">&raquo;</span>';
                elements += '<span class="sr-only">Próximo</span>';
                elements += '</a>';
                elements += '</li>';
                $('#pagination').append(elements);

                $('.itemPagination').click(function(event){
                    
                    event.preventDefault();
                    value = $(this).val();
                    let lStorageOffset = parseInt(localStorage.getItem('offset'));    
                    let lStorageLimit = parseInt(localStorage.getItem('limit'));  
                    let lStorageType = parseInt(localStorage.getItem('type'));  
                    let newOffset = value*20;

                    if (localStorage.getItem('type') == null) {
                        typeGet = null;
                    } else {
                        typeGet = localStorage.getItem('type') ;
                    }

                    localStorage.setItem('offset', newOffset);
                    localStorage.setItem('limit', lStorageLimit);

                    if (localStorage.getItem('offset') == 0) {
                        $('#preview').addClass('d-none');  
                    } else if (localStorage.getItem('offset') > 0) {
                        $('#preview').removeClass('d-none');
                    }

                    if (value != dataNumber) {
                        $('#next').removeClass('d-none');
                    } else {
                        $('#next').addClass('d-none');
                    }

                });

                $('.preview').click(function(event){
                    event.preventDefault();
                    let lStorageOffset = localStorage.getItem('offset');
                    localStorage.setItem('offset', lStorageOffset-20);

                    if (localStorage.getItem('offset') == 0) {
                        $('#preview').addClass('d-none');  
                    } else if (localStorage.getItem('offset') > 0) {
                        $('#preview').removeClass('d-none');
                    }
                    
                    if (parseInt(localStorage.getItem('offset')) == parseInt(dataNumber)*20) {
                        $('#next').addClass('d-none');
                    } else {
                        $('#next').removeClass('d-none');
                    }
                });

                $('.next').click(function(event){
                    event.preventDefault();
                    let lStorageOffset = parseInt(localStorage.getItem('offset'));
                    localStorage.setItem('offset', lStorageOffset+20);

                    if (localStorage.getItem('offset') == 0) {
                        $('#preview').addClass('d-none');    
                    } else if (localStorage.getItem('offset') > 0) {
                        $('#preview').removeClass('d-none');
                    }
                    
                    if (parseInt(localStorage.getItem('offset')) == parseInt(dataNumber)*20) { 
                        $('#next').addClass('d-none');
                    } else {
                        $('#next').removeClass('d-none');
                    }
                });

            } else {    
                return false;
            }
        },
        orderItemsByLine: function(item, itemByLine){
            let objResponsiveOdered = {};
            numRows = Math.ceil(item.length / itemByLine);
            offset = 0;
            for (i=0; i<numRows; i++) {
                objResponsiveOdered[i] = {};
                for (j=0; j<itemByLine; j++) {
                    objResponsiveOdered[i][j] = item[offset+j];
                }
                offset += itemByLine;
            }
            return JSON.stringify(objResponsiveOdered);
        },
        writeListPokemons: function(dataListPokemons) {
            let tmpMyListPokemons = [];
        
            $(dataListPokemons).each(function(i, value) {
                tmpMyListPokemons.push(JSON.parse(value));
            });
        
            let myListPokemons = JSON.parse(this.orderItemsByLine(tmpMyListPokemons, 4));
            let sizeMyListPokemons = Object.getOwnPropertyNames(myListPokemons).length;
        
            totItems = dataListPokemons.length;
            
            if (totItems > 4) {
                tmpMaxCols = 4;
            } else {
                tmpMaxCols = dataListPokemons.length;
            }
            
            $('#loadListPoke').html('');
            
            if (dataListPokemons.length > 0) {
                for (row=0; row < sizeMyListPokemons; row++) {
                    pokeDetails = '<div class="row p-0 m-0 m-sm-4 m-md-4 m-lg-4 m-xl-4">';
                    for (col=0; col < tmpMaxCols; col++) { 
                        pokeDetails += '<div class="col-12 col-sm-3 col-lg-3 col-xl-3 pokeDetails">';
                        pokeDetails += '<a class="linkPokeDetails" href="#">';
                        pokeDetails += '<div class="card text-center p-3 rounded">';
                        pokeDetails += '<img class="card-img-top mb-0 w-25 mx-auto" src="'+myListPokemons[row][col].sprite+'" alt="Imagem de capa do card" width="50">';
                        pokeDetails += '<div class="card-body p-0">';
                        pokeDetails += '<h5 class="card-title text-dark font-weight-bold text-capitalize">'+myListPokemons[row][col].name+'</h5>';
                        pokeDetails += '<h6 class="card-text text-muted small text-lowercase">'+myListPokemons[row][col].flavor_text_entries+'</h6>';
                        pokeDetails += '</div>';
                        pokeDetails += '</div>';
                        pokeDetails += '</a>';
                        pokeDetails += '</div>';
                    }
                    pokeDetails += '</div>';
                    totItems -= 4;
                    if (totItems >= 4) {
                        tmpMaxCols = 4;
                    } else {
                        tmpMaxCols = totItems;
                    }
                    $('#loadListPoke').append(pokeDetails);
                }
            } else {
                $('#loadListPoke').append('<p class="text-center text-dark">Items not found</p>');
            }
        }
    }
    
    // let getPokeListByGeration = core.getPokemonByGeration(1);
    // let listPokemonsForSearch = [];
    // $(getPokeListByGeration).each(function(i, value){
    //     listPokemonsForSearch.push(value.name);
    // }); 

    // $('#pokeSearch').keyup(function(){
    //     var value = $(this).val().toLowerCase();
    //     $('#loadListPoke *').filter(function(index) {
    //       $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
    //     });
    // });

    //search types and load select
    let resultGetListTypesPokemons = core.getPokemonTypesByGeration(1);
    $(resultGetListTypesPokemons).each(function(i, value){
        $('#pokeTypes').append('<option value="'+value.name+'">'+value.name.substr(0,1).toUpperCase()+value.name.substr(1)+'</option>');
    });

    $('#pokeTypes').change(function(){
        value = $(this).val().toLowerCase();
        localStorage.setItem('type', value);
        if ( (localStorage.getItem('type') == null) || (localStorage.getItem('type') == 'all')) {
            resultGetListPokemons = core.getPokemonList(1,0,20, null, true);
        } else {
            resultGetListPokemons = core.getPokemonList(1,0,20, value, true);
        }
        core.writeListPokemons(resultGetListPokemons);
    });

    // Write pokemons onload
    if ( (localStorage.getItem('type') == null) || (localStorage.getItem('type') == 'all')) {
        resultGetListPokemons = core.getPokemonList(1, 0, 20, null, true);
    } else {
        resultGetListPokemons = core.getPokemonList(1, 0, 20, localStorage.getItem('type'), true);
        $('#pokeTypes').val(localStorage.getItem('type'));
    }
    // console.log(resultGetListPokemons);
    core.writeListPokemons(resultGetListPokemons);

});
    